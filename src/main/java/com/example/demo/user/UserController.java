package com.example.demo.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/user")
public class UserController {
    @GetMapping
    public List<User> getUser() {
        return List.of(
                new User(
                        1L,
                        "Brian Tarno",
                        546372L

                ),
                new User(
                        2L,
                        "Edwin Kipkorir",
                        453627L
                )
        );
    }
}
