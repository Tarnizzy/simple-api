package com.example.demo.user;

public class User {
    private Long id;
    private String name;
    private Long msisdn;

    public User() {
    }

    public User(Long id,
                String name,
                Long msisdn) {
        this.id = id;
        this.name = name;
        this.msisdn = msisdn;
    }

//    public User(String name, Long msisdn) {
//        this.name = name;
//        this.msisdn = msisdn;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", msisdn=" + msisdn +
                '}';
    }
}
